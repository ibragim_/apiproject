﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiProject.Models;

namespace WebApiProject.Services
{
    public class AuthorsService : IAuthorService
    {
        private readonly ApplicationDbContext _context;

        public AuthorsService(ApplicationDbContext context)
        {
            context = _context;
        }
        public Task<bool> CreateAuthor(Author author)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteAuthor(int Id)
        {
            throw new NotImplementedException();
        }

        public Task<Author> GetAuthor(int id)
        {
            throw new NotImplementedException();
        }

        public Task<List<Author>> GetAuthors()
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateAuthor(Author author)
        {
            throw new NotImplementedException();
        }
    }
}
