﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiProject.Interfaces
{
    interface IEntity <T>
    {
        public int Id { get; set; }

    }
}
