﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiProject.Models;
using WebApiProject.Services;

namespace WebApiProject.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        private readonly IAuthorService _authorSevice;


        public AuthorsController(IAuthorService authorSevice)
        {
            _authorSevice = authorSevice;
        }

        [HttpGet]
        public async Task<ActionResult<List<Author>>> Get()
        {
            var authors = await _authorSevice.GetAuthors();
            return Ok(authors);
        }

        //[HttpGet]
        //public async Task<ActionResult<Author>> Get(int id)
        //{
         
        //}
    }
}
